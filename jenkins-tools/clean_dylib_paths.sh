#!/bin/sh
# this script helps to remove absolute paths to dynamic libraries on OSX
# the easiest way to execute it on a full folder is:
# find . -exec $AMSHOME/Utils/clean_osx_dylib_paths.sh {} ";"

for prefix in /Users/ /opt/intel /opt/local; do
  for filepath in $* ; do
    filename=`basename $filepath`

# check if there are references to $prefix
    otoolout=`otool -L $filepath 2>/dev/null | grep -v framework | grep -m 1 "^[[:blank:]]*$prefix"`
    while test ! -z "$otoolout"; do
      old=`echo $otoolout | awk -F ' ' '{print $1}'`
      echo "Stripping $prefix from $filepath: $otoolout"
      new=`basename $old`
      strippedname=`echo $filename | cut -d'.' -f1`
      strippednew=`echo $new | cut -d'.' -f1`
#    echo $filename $new
#    echo $strippedname $strippednew
      if test ! -w $filepath; then
        chmod u+w $filepath
        removewrite=true
      else
        removewrite=false
      fi
      if test "$strippedname" = "$strippednew"; then
        # we need to use -id instead of -change
        install_name_tool -id @rpath/$new $filepath
      else
        install_name_tool -change $old @rpath/$new $filepath
      fi
      if test removewrite = true; then
        chmod u-w $filepath
      fi
      otoolout=`otool -L $filepath 2>/dev/null | grep -m 1 "^[[:blank:]]*$prefix"`
    done
  done
done


