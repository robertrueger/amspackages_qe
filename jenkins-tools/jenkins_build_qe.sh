set -e 
# Build dependencies

case $(uname) in

Linux)

  if [ "$QEBUILD_SKIP_LINUX" = "true" ]; then
    exit 255
  fi

  . /opt/intel/oneapi/compiler/2021.4.0/env/vars.sh
  . /opt/intel/oneapi/mpi/2021.14/env/vars.sh
  . /opt/intel/oneapi/mkl/2021.4.0/env/vars.sh

  export PATH=/opt/centos7/gcc-10.3.0/bin:$PATH LD_LIBRARY_PATH=/opt/centos7/gcc-10.3.0/lib64:$LD_LIBRARY_PATH
  export PATH=/opt/centos7/binutils-2.30/bin:$PATH

  function build_qe(){
  
    cd "$WORKSPACE"
	  rm -rf ./build
    rm -rf ./dist
    mkdir build && cd build || exit 1

    /home/builder/bas/QE/utils/cmake-3.25.1/bin/cmake \
      -DGIT_EXECUTABLE=/home/builder/bas/QE/utils/git-2.39.0-build/bin/git \
      -DCMAKE_INSTALL_PREFIX="$WORKSPACE"/dist/qe \
      -DQE_ENABLE_AMSPIPE=ON \
      -DQE_ENABLE_OPENMP=ON \
      -DCMAKE_C_COMPILER=/opt/centos7/gcc-10.3.0/bin/gcc \
      -DCMAKE_Fortran_COMPILER=mpiifort \
      -DQE_ENABLE_SCALAPACK=ON \
      "$WORKSPACE/q-e"

    make -j 8
    make install
    
    # Install command is kind of broken, and doesn't add all binaries
    cp -vr $WORKSPACE/build/bin/*.x "$WORKSPACE"/dist/qe/bin/.
    cd "$WORKSPACE/dist/qe" || exit 1
    # Linux dependencies are a bunch of different folders containing libraries, and nested directories with more libraries (esp. mpi stuff)
    # A few of these nested dirs need to be in the LD_LIBRARY_PATH at runtime because we have not tweaked rpaths
    tar xzvf "$WORKSPACE"/jenkins-tools/qe-depends-linux.tgz
    cd "$WORKSPACE/dist/" || exit 1
    tar cfz qe-amspipe-linux.tgz qe
    scp qe-amspipe-linux.tgz testadf@master.scm.com:snapshots/quantum_espresso/.
  }

  ;;
Darwin)

  arch=$(uname -p)
  # load git lfs and other stuff
  export PATH="$PATH:/usr/local/bin/"

  if [ "$arch" = "i386" ]; then

    if [ "$QEBUILD_SKIP_MACOS_X86" = "true" ]; then
        exit 255
    fi
    
    . /opt/intel/oneapi/compiler/2021.4.0/env/vars.sh
    . /opt/intel/oneapi/mkl/2021.4.0/env/vars.sh

    function build_qe() {
      
      cd "$WORKSPACE"
	    rm -rf ./build
      rm -rf ./dist
      mkdir build && cd build || exit 1

      /Users/builder/software/cmake-3.25.2-macos-universal/CMake.app/Contents/bin/cmake \
        -DCMAKE_INSTALL_PREFIX="$PWD"/../dist/qe \
        -DQE_ENABLE_MPI=OFF \
        -DQE_ENABLE_OPENMP=ON \
        -DQE_ENABLE_AMSPIPE=ON \
        -DCMAKE_C_COMPILER=/usr/bin/clang \
        -DCMAKE_CXX_COMPILER=/usr/bin/clang++ \
        -DOpenMP_C_FLAGS=-liomp5 \
        -DOpenMP_C_LIB_NAMES=iomp5 \
        -DCMAKE_Fortran_COMPILER=ifort \
        "$WORKSPACE/q-e"

      make -j 8 || exit 1
      make install || exit 1

      # Install command is kind of broken, and doesn't add all binaries
      cp -vr $WORKSPACE/build/bin/*.x "$WORKSPACE"/dist/qe/bin/.
      cd "$WORKSPACE/dist/" || exit 1
      chmod +x "$WORKSPACE"/jenkins-tools/clean_dylib_paths.sh    
      find qe -exec "$WORKSPACE"/jenkins-tools/clean_dylib_paths.sh {} ";"    
      cd qe || exit 1
      # Darwin depends are a bunch of dylib files that need to be added to the lib folder
      # This lib folder NEEDS to be added to DYLD_LIBRARY_PATH at runtime because we have not tweaked loader paths
      tar xvzf "$WORKSPACE"/jenkins-tools/qe-depends-darwin-x86.tgz
      cd "$WORKSPACE/dist/" || exit 1
      tar cfz qe-amspipe-darwin-x86.tgz qe
      scp qe-amspipe-darwin-x86.tgz testadf@master.scm.com:snapshots/quantum_espresso/.
    }
  elif [ "$arch" = "arm" ] ; then 

    if [ "$QEBUILD_SKIP_MACOS_ARM" = "true" ]; then
        exit 255
    fi

    function build_qe() {
          
      cd "$WORKSPACE"
	    rm -rf ./build
      rm -rf ./dist
      mkdir build && cd build || exit 1
      
      /Applications/CMake.app/Contents/bin/cmake \
        -DCMAKE_INSTALL_PREFIX="$PWD"/../dist/qe \
        -DCMAKE_BUILD_TYPE=Release \
        -DBLA_VENDOR=Apple \
        -DQE_ENABLE_MPI=OFF \
        -DQE_ENABLE_OPENMP=ON \
        -DQE_ENABLE_AMSPIPE=ON \
        -DQE_FFTW_VENDOR=Internal \
        -DCMAKE_C_COMPILER=/opt/local/bin/clang-mp-14 \
        -DCMAKE_CXX_COMPILER=/opt/local/bin/clang++-mp-14 \
        -DCMAKE_Fortran_COMPILER=/opt/local/bin/gfortran-mp-12 \
        "$WORKSPACE/q-e"

      # use cmake not make, or it will rerun cmake and fail to pick up blas...
      /Applications/CMake.app/Contents/bin/cmake --build . -j4 --config Release --verbose || exit 1
      /Applications/CMake.app/Contents/bin/cmake --install .  --config Release --verbose || exit 1

      # Install command is kind of broken, and doesn't add all binaries
      cp -vr $WORKSPACE/build/bin/*.x "$WORKSPACE"/dist/qe/bin/.
      cd "$WORKSPACE/dist/" || exit 1
      chmod +x "$WORKSPACE"/jenkins-tools/clean_dylib_paths.sh
      find qe -exec "$WORKSPACE"/jenkins-tools/clean_dylib_paths.sh {} ";"    
      cd "$WORKSPACE/dist/qe" || exit 1
      # Darwin depends are a bunch of dylib files that need to be added to the lib folder
      # This lib folder NEEDS to be added to DYLD_LIBRARY_PATH at runtime
      tar xvzf "$WORKSPACE"/jenkins-tools/qe-depends-darwin-arm64.tgz
      cd "$WORKSPACE/dist/" || exit 1
      tar cfz qe-amspipe-darwin-arm64.tgz qe
      scp qe-amspipe-darwin-arm64.tgz testadf@master.scm.com:snapshots/quantum_espresso/.
    }

  fi
  
  

  ;;
Windows|MINGW64_NT-10.0|MSYS_NT-10.0)

  if [ "$QEBUILD_SKIP_WINDOWS" = "true" ]; then
    exit 255
  fi
  
  # set up for OneAPI 2023.2.0 / Visual Studio 2019
  echo  "OneAPI 2023.2.0 ifx, MKL, MS-MPI"
  export CMPLR_ROOT='C:/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0'
  export MKLROOT='C:/Program Files (x86)/Intel/oneAPI/mkl/2023.2.0'

  export CPATH="$MKLROOT/include;$CMPLR_ROOT/windows/include;$CMPLR_ROOT/windows/compiler/include;$CMPLR_ROOT/windows/compiler/include/intel64;"
  export IFCPATH='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/ifc/x64'
  export INCLUDE="$MKLROOT/include;$CMPLR_ROOT/windows/include;$CMPLR_ROOT/windows/compiler/include;$CMPLR_ROOT/windows/compiler/include/intel64;C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/ATLMFC/include;C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/include;C:/Program Files (x86)/Windows Kits/NETFXSDK/4.8/include/um;C:/Program Files (x86)/Windows Kits/10/include/10.0.19041.0/ucrt;C:/Program Files (x86)/Windows Kits/10/include/10.0.19041.0/shared;C:/Program Files (x86)/Windows Kits/10/include/10.0.19041.0/um;C:/Program Files (x86)/Windows Kits/10/include/10.0.19041.0/winrt;C:/Program Files (x86)/Windows Kits/10/include/10.0.19041.0/cppwinrt"
  export INTEL_TARGET_ARCH=intel64
  export INTEL_TARGET_PLATFORM=windows
  export LIB="$MKLROOT/lib/intel64;$CMPLR_ROOT/windows/compiler/lib;$CMPLR_ROOT/windows/compiler/lib/intel64_win;$CMPLR_ROOT/windows/lib;$CMPLR_ROOT/windows/lib/x64;C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/ATLMFC/lib/x64;C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/lib/x64;C:/Program Files (x86)/Windows Kits/NETFXSDK/4.8/lib/um/x64;C:/Program Files (x86)/Windows Kits/10/lib/10.0.19041.0/ucrt/x64;C:/Program Files (x86)/Windows Kits/10/lib/10.0.19041.0/um/x64"
  export LIBPATH='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/ATLMFC/lib/x64;C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/lib/x64;C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/lib/x86/store/references;C:/Program Files (x86)/Windows Kits/10/UnionMetadata/10.0.19041.0;C:/Program Files (x86)/Windows Kits/10/References/10.0.19041.0;C:/Windows/Microsoft.NET/Framework64/v4.0.30319'
  export MSVS_VAR_SCRIPT='"C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build/vcvarsall.bat"'
  export MSVS_VAR_SCRIPT_DIR='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Auxiliary/Build'
  export NLSPATH="$MKLROOT/lib/intel64;"
  export OCL_ICD_FILENAMES="$CMPLR_ROOT/windows/lib/emu/intelocl64_emu.dll;$CMPLR_ROOT/windows/lib/x64/intelocl64.dll"
  export ONEAPI_ROOT='C:/Program Files (x86)/Intel/oneAPI/'
  export PATH='/c/Program Files (x86)/Intel/oneAPI/mkl/2023.2.0/redist/intel64:/c/Program Files (x86)/Intel/oneAPI/mkl/2023.2.0/bin/intel64:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/bin:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/lib:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/bin/intel64:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/redist/intel64_win/compiler:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/lib/oclfpga/host/windows64/bin:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/lib/oclfpga/llvm/aocl-bin:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/lib/oclfpga/windows64/bin:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/lib/oclfpga/bin:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/bin/HostX64/x64:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/IDE/VC/VCPackages:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/IDE/CommonExtensions/Microsoft/TestWindow:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/IDE/CommonExtensions/Microsoft/TeamFoundation/Team Explorer:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/MSBuild/Current/bin/Roslyn:/c/Program Files (x86)/Microsoft SDKs/Windows/v10.0A/bin/NETFX 4.8 Tools/x64:/c/Program Files (x86)/HTML Help Workshop:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/Tools/devinit:/c/Program Files (x86)/Windows Kits/10/bin/10.0.19041.0/x64:/c/Program Files (x86)/Windows Kits/10/bin/x64:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/MSBuild/Current/Bin:/c/Windows/Microsoft.NET/Framework64/v4.0.30319:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/IDE:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/Tools:/c/Program Files (x86)/Intel/oneAPI/mpi/latest/bin:/c/Program Files (x86)/Intel/oneAPI/mpi/latest/bin/release:/c/Program Files (x86)/Intel/oneAPI/mpi/latest/libfabric/bin:/c/Program Files (x86)/Intel/oneAPI/mpi/latest/libfabric/bin/utils:/c/Program Files (x86)/Intel/oneAPI/tbb/latest/redist/intel64/vc_mt:/c/Program Files (x86)/Intel/oneAPI/tbb/latest/redist/ia32/vc_mt:/c/Program Files (x86)/Intel/oneAPI/compiler/latest/windows/redist/intel64_win/compiler:/c/Program Files (x86)/Intel/oneAPI/compiler/latest/windows/redist/ia32_win/compiler:/c/Program Files/CollabNet/Subversion Client:/c/Program Files/Zulu/zulu-11-jre/bin:/usr/bin:/c/Windows/system32:/c/Windows:/c/Windows/System32/Wbem:/c/Windows/System32/WindowsPowerShell/v1.0:/c/Windows/System32/OpenSSH:/c/adfsrc/TOOLS/Putty:/c/Users/testadf/AppData/Local/Microsoft/WindowsApps:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/Llvm/x64/bin:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/IDE/CommonExtensions/Microsoft/CMake/CMake/bin:/c/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/IDE/CommonExtensions/Microsoft/CMake/Ninja:/c/Program Files (x86)/Intel/oneAPI/compiler/2023.2.0/windows/lib/ocloc'
  export PKG_CONFIG_PATH="$MKLROOT/lib/pkgconfig;$CMPLR_ROOT/lib/pkgconfig;"
  export TARGET_VS=vs2019
  export TARGET_VS_ARCH=amd64
  export UCRTVersion=10.0.19041.0
  export VCIDEInstallDir='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/IDE/VC/'
  export VCINSTALLDIR='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/'
  export VCToolsInstallDir='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/'
  export VCToolsRedistDir='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Redist/MSVC/14.29.30133/'
  export VCToolsVersion=14.29.30133
  export VS160COMNTOOLS='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/Common7/Tools/'
  export VS2019INSTALLDIR='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional'
  export VSCMD_ARG_HOST_ARCH=x64
  export VSCMD_ARG_TGT_ARCH=x64
  export VSCMD_ARG_app_plat=Desktop
  export VSCMD_VER=16.11.32
  export VSINSTALLDIR='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/'
  export VisualStudioVersion=16.0
  export WindowsLibPath='C:/Program Files (x86)/Windows Kits/10/UnionMetadata/10.0.19041.0;C:/Program Files (x86)/Windows Kits/10/References/10.0.19041.0'
  export WindowsSDKLibVersion='10.0.19041.0/'
  export WindowsSDKVersion='10.0.19041.0/'
  export WindowsSDK_ExecutablePath_x64='C:/Program Files (x86)/Microsoft SDKs/Windows/v10.0A/bin/NETFX 4.8 Tools/x64/'
  export WindowsSDK_ExecutablePath_x86='C:/Program Files (x86)/Microsoft SDKs/Windows/v10.0A/bin/NETFX 4.8 Tools/'
  export WindowsSdkBinPath='C:/Program Files (x86)/Windows Kits/10/bin/'
  export WindowsSdkDir='C:/Program Files (x86)/Windows Kits/10/'
  export WindowsSdkVerBinPath='C:/Program Files (x86)/Windows Kits/10/bin/10.0.19041.0/'
  export __MS_VC_INSTALL_PATH='C:/Program Files (x86)/Microsoft Visual Studio/2019/Professional/VC/Tools/MSVC/14.29.30133/'
  export __VSCMD_PREINIT_PATH='C:/Program Files (x86)/Intel/oneAPI/mpi/latest/bin;C:/Program Files (x86)/Intel/oneAPI/mpi/latest/bin/release;C:/Program Files (x86)/Intel/oneAPI/mpi/latest/libfabric/bin;C:/Program Files (x86)/Intel/oneAPI/mpi/latest/libfabric/bin/utils;C:/Program Files (x86)/Intel/oneAPI/tbb/latest/redist/intel64/vc_mt;C:/Program Files (x86)/Intel/oneAPI/tbb/latest/redist/ia32/vc_mt;C:/Program Files (x86)/Intel/oneAPI/compiler/latest/windows/redist/intel64_win/compiler;C:/Program Files (x86)/Intel/oneAPI/compiler/latest/windows/redist/ia32_win/compiler;C:/Program Files/CollabNet/Subversion Client;C:/Program Files/Zulu/zulu-11-jre/bin;C:/adfsrc/TOOLS/msys2-x86_64-20161025/usr/bin;C:/Windows/system32;C:/Windows;C:/Windows/System32/Wbem;C:/Windows/System32/WindowsPowerShell/v1.0;C:/Windows/System32/OpenSSH;C:/adfsrc/TOOLS/Putty'


  
  function build_qe() {  
    
    workspace_cyg=$(cygpath --unix "$WORKSPACE")
    workspace_win=$(cygpath --mixed "$WORKSPACE")
    cd "$workspace_cyg"
	  rm -rf ./build
    rm -rf ./dist

    # Unpack MS-MPI including headers and import libs needed for the build
    mkdir -p dist/qe && cd dist/qe || exit 1
    tar xvzf "$workspace_cyg/jenkins-tools/qe-depends-windows.tgz"
    cd "$workspace_cyg"

    mkdir build && cd build || exit 1

    # Disable all path translation to protect compiler arguments with leading slashes
    # No POSIX-style paths are thus allowed beyond this point
    export MSYS2_ARG_CONV_EXCL="*"

    MSMPI_ROOT="$workspace_win/dist/qe/msmpi"
    MPIEXEC=$MSMPI_ROOT/Bin/mpiexec.exe
    # These two need to be exported for FindMPI.cmake
    export MSMPI_INC=$MSMPI_ROOT/Include
    export MSMPI_LIB64=$MSMPI_ROOT/Lib/x64

    # Note: We have to build with Ninja, other build tools can't work with disabled path translation
    # Note: Need to define random=rand, srandom=srand
    # Note: Need to define M_PI because its not standard
    # Note: /F1073741824 sets the maximum stack size for all executables to 1 GiB
    # Note: -DMPI_Fortran_ADDITIONAL_INCLUDE_DIRS is a workaround for CMake bug #21257
    C:/adfsrc/TOOLS/cmake-3.25.2/bin/cmake.exe \
        -DCMAKE_INSTALL_PREFIX="$workspace_win/dist/qe" \
        -DGIT_EXECUTABLE=C:/adfsrc/TOOLS/Git/bin/git.exe \
        -DQE_ENABLE_MPI=ON \
        -DMPIEXEC_EXECUTABLE="$MPIEXEC" \
        -DMPI_GUESS_LIBRARY_NAME=MSMPI \
        -DMPI_Fortran_ADDITIONAL_INCLUDE_DIRS="$MSMPI_INC;$MSMPI_INC/x64" \
        -DQE_ENABLE_OPENMP=OFF \
        -DQE_ENABLE_AMSPIPE=ON \
        -DQE_FFTW_VENDOR=Internal \
        -DCMAKE_C_FLAGS="-DM_PI=3.14159265358979323846 -Drandom=rand -Dsrandom=srand" \
        -DCMAKE_EXE_LINKER_FLAGS="/F1073741824" \
        -DCMAKE_Fortran_COMPILER=ifx \
        -DCMAKE_C_COMPILER=cl \
        -DCMAKE_CXX_COMPILER=cl \
        -DCMAKE_BUILD_TYPE=Release \
        -G "Ninja" \
        "$workspace_win/q-e"

    # Patch the devxlib source to be windows compatible
    cd "$workspace_win/q-e/external/devxlib"
    C:/adfsrc/TOOLS/Git/bin/git.exe checkout -- src/timer.c
    C:/adfsrc/TOOLS/Git/bin/git.exe apply "$workspace_win/jenkins-tools/devxlib-Make-wallclock_c-compatible-with-native-Windows-C-library.patch"
    cd "$workspace_win/build/"

    cmake --build . -j4 --verbose

    # Work around a bug in qe_install_target which looks for Fortran modules in a C-only target
    # and fails because the module directory is missing, breaking the whole install step
    mkdir UtilXlib/mod/qe_utilx_c Modules/mod/qe_modules_c XClib/mod/qe_libbeef

    cmake --install . --verbose

    # We're done building, so we can re-enable path translation
    unset MSYS2_ARG_CONV_EXCL
    
    # Install command is kind of broken, and doesn't add all binaries
    cp -vr "$workspace_cyg"/build/bin/* "$workspace_cyg"/dist/qe/bin/.
    cd "$workspace_cyg/dist/qe/bin" || exit 1
            
    # the depends for windows are a bunch of DLL files, which should be added to the bin folder
    # this bin folder then NEEDS to be in PATH at run time.    
    cp "$MKLROOT"/redist/intel64/*.dll ./
    rm -f *_ilp64*.dll mkl_pgi_*.dll mkl_blacs_intelmpi_*.dll mkl_sycl*.dll
    cp "$CMPLR_ROOT"/windows/redist/intel64_win/compiler/*md.dll ./

    cd "$workspace_cyg/dist/"
    tar cfz qe-amspipe-windows-x86.tgz qe
    pscp -i $KEY_LOCATION qe-amspipe-windows-x86.tgz testadf@master.scm.com:snapshots/quantum_espresso/.
  }

 

  ;;

*) echo "Unknown OS/system"
   exit 1 ;;

esac



